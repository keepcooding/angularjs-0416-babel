
// Componente raíz.
angular
    .module("cookbook")
    .component("raiz", {

        // Con $routeConfig establecemos las rutas que se navegan en este componente.
        $routeConfig: [{
            name: "MisRecetas",
            path: "/mis-recetas",
            component: "misRecetas",
            useAsDefault: true
        }, {
            name: "NuevaReceta",
            path: "/nueva-receta",
            component: "nuevaReceta"
        }, {
            name: "DetalleReceta",
            path: "/detalle-receta/:idReceta",
            component: "detalleReceta"
        }],

        // Con templateUrl referenciamos la vista asociada al componente.
        templateUrl: "views/raiz.html",

        // Con controller establecemos la lógica del componente.
        controller: function($rootRouter, $pubsub) {

            var self = this;

            self.$onInit = function() {
                self.misRecetasSeleccionada = true;
                self.nuevaRecetaSeleccionada = false;
            };

            // Nos subscribimos a la navegación de secciones.
            $pubsub.$subscribe("SeccionNavegada", function(datos) {
                self.misRecetasSeleccionada = datos === "MisRecetas";
                self.nuevaRecetaSeleccionada = datos === "NuevaReceta";
            });

            // Seleccionamos la pestaña 'Mis Recetas' y navegamos a dicha sección.
            self.navegarMisRecetas = function() {
                $rootRouter.navigate(["MisRecetas"]);
            };
            
            // Seleccionamos la pestaña 'Nueva Receta' y navegamos a dicha sección.
            self.navegarNuevaReceta = function() {
                $rootRouter.navigate(["NuevaReceta"]);
            };
        }
    });