
// Componente 'misRecetas' que muestra la colección de recetas.
angular
    .module("cookbook")
    .component("misRecetas", {
        
        templateUrl: "views/mis-recetas.html",
        
        controller: function(ServicioRecetas, $filter, $pubsub, $window) {
            
            var self = this;

            // Obtenemos la lista de recetas.
            self.$onInit = function() {
                
                // Notifico al componente padre que he navegado
                // correctamente la sección 'Mis Recetas'.
                $pubsub.$publish("SeccionNavegada", "MisRecetas");
                
                ServicioRecetas.obtenerRecetas().then(
                    // Este manejador se ejecuta si la petición fue correcta.
                    function(respuesta) {
                        self.recetas = respuesta.data;
                    },
                    // Este manejador se ejecuta si la petición dio algún tipo de error.
                    function() {
                        alert("Hubo un error en la petición.");
                    }
                );
            };

            // Eliminamos la receta indicada.
            self.eliminarReceta = function(id) {

                // Preguntamos al usuario si confirma la eliminación.
                if ($window.confirm("¿Está seguro de que desea eliminar la receta?")) {

                    // Llamamos al servicio para eliminar la
                    // receta con el identificador indicado.
                    ServicioRecetas.borrarReceta(id).then(
                        // Este manejador se ejecuta si la petición fue correcta.
                        function (respuesta) {

                            // Buscamos la receta eliminada en la colección.
                            var filtradas = $filter("filter")(self.recetas, {"id": id});

                            // Comprobamos si hemos encontrado la receta.
                            if (filtradas.length === 1) {

                                // Obtengo el índice de la receta en la colección.
                                var indice = self.recetas.indexOf(filtradas[0]);

                                // Eliminamos la receta de la colección.
                                self.recetas.splice(indice, 1);
                            }
                        },
                        // Este manejador se ejecuta si la petición dio algún tipo de error.
                        function () {
                            alert("Hubo un error en la petición.");
                        }
                    );
                }
            };
        }
    });