
angular
    .module("cookbook")
    .component("ingredientes", {
        bindings: {
            ingredientes: "<",
            alAgregarIngrediente: "&"
        },
        templateUrl: "views/ingredientes.html",
        controller: function() {

            var self = this;

            // Añadimos un ingrediente a la colección en caso de pulsar 'intro'.
            self.alPulsarTecla = function(evento) {

                // Obtenemos el código de la tecla pulsada.
                var tecla = evento.which || evento.keyCode;

                // Comprobamos si se ha pulsado la tecla 'intro'.
                if (tecla === 13) {

                    // Creo el objeto ingrediente.
                    var ingrediente = {
                        nombre: self.nuevoIngrediente,
                        cantidad: 1
                    };

                    // Notifico el ingrediente hacia el componente padre.
                    self.alAgregarIngrediente({
                        "ingrediente": ingrediente
                    });

                    // Limpio el valor de la caja.
                    self.nuevoIngrediente = "";
                }
            };
        }
    });