
// Componente 'nuevaReceta' que tiene la funcionalidad de alta de nuevas recetas.
angular
    .module("cookbook")
    .component("nuevaReceta", {

        // Enlazamos el $router correspondiente a este componente
        // para poder navegar a otros desde el código funcional.
        bindings: {
            $router: "<"
        },

        templateUrl: "views/nueva-receta.html",

        controller: function(ServicioRecetas, $pubsub) {

            var self = this;

            // Inicializamos la receta.
            self.$onInit = function() {

                // Notifico al componente padre que he navegado
                // correctamente la sección 'Nueva Receta'.
                $pubsub.$publish("SeccionNavegada", "NuevaReceta");

                self.receta = {
                    titulo: "",
                    ingredientes: []
                };

                self.imagen = null;
            };

            // Añadimos un ingrediente a la receta.
            self.agregarIngrediente = function(ingrediente) {

                self.receta.ingredientes.push(ingrediente);
            };

            // Seleccionamos la imagen.
            self.imagenSeleccionada = function(imagen) {

                self.imagen = imagen;
            };

            // Crea una nueva receta.
            self.crearReceta = function() {

                // Compruebo que la caja de texto tenga valor.
                if (self.receta.titulo) {

                    // Llamamos al servicio para crear la receta.
                    ServicioRecetas.crearReceta(self.receta, self.imagen).then(

                        // Este manejador se ejecuta si la petición fue correcta.
                        function(respuesta) {
                            // Una vez se crea la receta, llevamos al usuario a la lista general.
                            self.$router.navigate(["/MisRecetas"]);
                        },
                        // Este manejador se ejecuta si la petición dio algún tipo de error.
                        function() {
                            alert("Hubo un error en la petición.")
                        }
                    );
                }
            };
        }
    });