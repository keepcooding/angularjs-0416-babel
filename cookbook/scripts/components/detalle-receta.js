
// Componente 'detalleReceta'.
angular
    .module("cookbook")
    .component("detalleReceta", {
        bindings: {
            $router: "<"
        },
        templateUrl: "views/detalle-receta.html",
        controller: function(ServicioRecetas, $window, $pubsub) {
            
            var self = this;

            // Notificamos la deselección de pestañas.
            self.$onInit = function() {

                $pubsub.$publish("SeccionNavegada", "DetalleReceta");
            };

            // Al navegar la ruta.
            self.$routerOnActivate = function(rutaNavegada) {

                // Recuperamos el identificador de la receta.
                var idReceta = rutaNavegada.params.idReceta;

                // Obtenemos la receta.
                ServicioRecetas
                    .obtenerReceta(idReceta)
                    .then(
                        function(respuesta) {
                            self.receta = respuesta.data;
                        },
                        function() {
                            alert("Ha habido un error al obtener la receta.");
                        }
                    );
            };

            // Exponemos en la vista el obtenerRutaImagen().
            self.obtenerRutaImagen = ServicioRecetas.obtenerRutaImagen;

            // Manejador del botón eliminar.
            self.eliminarReceta = function(idReceta) {

                // Preguntamos al usuario si confirma la eliminación.
                if ($window.confirm("¿Está seguro de que desea eliminar la receta?")) {

                    ServicioRecetas
                        .borrarReceta(idReceta)
                        .then(
                            function() {
                                self.$router.navigate(["MisRecetas"]);
                            },
                            function() {
                                alert("Ha habido un error al eliminar la receta.");
                            }
                        );
                }
            };

            // Manejador del botón volver.
            self.volverAtras = function() {

                $window.history.back();
            };
        }
    });
