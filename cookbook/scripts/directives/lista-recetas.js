
// Directiva lista recetas.
angular
    .module("cookbook")
    .directive("listaRecetas", function(ServicioRecetas) {

        // Las directivas siempre devuelven un objeto.
        return {
            // Con restrict indicamos cómo usaremos la directiva.
            // E: la usamos como elemento.
            // A: la usamos como atributo de un elemento.
            restrict: "EA",
            // Con scope establecemos la interfaz de comunición
            // entre directiva y scope padre.
            scope: {
                // Con = establecemos un enlace bidireccional.
                coleccion: "=",
                // Con & no suscribimos a eventos.
                botonEliminarPulsado: "&"
            },
            // En templateUrl establecemos la ruta del documento
            // HTML que tiene la vista de la directiva.
            templateUrl: "views/lista-recetas.html",
            // En la fase link podemos establecer la lógica de la
            // directiva y manipular el DOM de la vista asociada.
            link: function(scope) {

                // Notificamos al controlador que se pulsó el botón
                // de eliminar de una receta.
                scope.eliminarReceta = function(id) {
                    scope.botonEliminarPulsado({ "id": id });
                };
                
                // Publicamos en el scope la función 'obtenerRutaImagen' para
                // que la vista asociada la tenga disponible.
                scope.obtenerRutaImagen = ServicioRecetas.obtenerRutaImagen;
            }
        };
    });