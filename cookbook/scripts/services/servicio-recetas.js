
// Servicio 'ServicioRecetas'.
angular
    .module("cookbook")
    .service("ServicioRecetas", function($http, Configuracion) {

        // Crea una nueva receta.
        this.crearReceta = function(receta, imagen) {

            var promesa;

            // Comprobamos si existe la imagen.
            if (imagen) {

                // Creamos un objeto datos de formulario.
                var datos = new FormData();

                // Añadimos la imagen a los datos de fomulario.
                datos.append("imagen", imagen);

                // Creamos un objeto de configuración. Necesitamos sobreescribir la cabecera
                // Content-Type, que por defecto es 'application/json'.
                var configuracion = {
                    "headers": {
                        "Content-Type": undefined
                    }
                };

                // Subimos la imagen al servidor.
                promesa = $http
                    .post(
                        Configuracion.urlServidor + "/upload",
                        datos,
                        configuracion
                    )
                    .then(
                        function(respuesta) {

                            // Asigno la ruta de la imagen subida al objeto 'receta'.
                            receta.rutaImagen = respuesta.data.path;

                            // Guardamos la receta ya con la ruta asignada.
                            return $http.post(Configuracion.urlServidor + "/api/recetas", receta);
                        }
                    );
            }
            else {

                // En caso de que no haya imagen, se almacena solo la receta.
                promesa = $http.post(Configuracion.urlServidor + "/api/recetas", receta);
            }

            return promesa;
        };

        // Obtener recetas.
        this.obtenerRecetas = function() {

            return $http.get(Configuracion.urlServidor + "/api/recetas");
        };

        // Eliminar receta.
        this.borrarReceta = function(id) {

            return $http.delete(Configuracion.urlServidor + "/api/recetas/" + id);
        };

        // Obtenemos la ruta absoluta de la imagen dada.
        this.obtenerRutaImagen = function(rutaRelativa) {

            return rutaRelativa ? (Configuracion.urlServidor + "/" + rutaRelativa) : null;
        };
        
        // Obtenemos el detalle de una receta individual.
        this.obtenerReceta = function(idReceta) {
            
            return $http.get(Configuracion.urlServidor + "/api/recetas/" + idReceta);
        };
    });