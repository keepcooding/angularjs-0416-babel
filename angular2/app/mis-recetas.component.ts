
import { Component, OnInit } from "@angular/core";
import { HTTP_PROVIDERS } from "@angular/http";
import { ServicioRecetas } from "./servicio-recetas.service";

@Component({
    template: `
        <div>
            <header>
                <h3>Mis Recetas</h3>
            </header>
            <ul>
                <li *ngFor="let receta of recetas">{{ receta.titulo }}</li>
            </ul>
        </div>
    `,
    providers: [
        ServicioRecetas,
        HTTP_PROVIDERS
    ]
})
export class MisRecetasComponent implements OnInit {

    recetas: any[];

    constructor(private _servicioRecetas: ServicioRecetas) { }

    ngOnInit() {
        this._servicioRecetas
            .obtenerRecetas()
            .subscribe(
                recetas => this.recetas = recetas,
                error => alert(error)
            );
    }
}