
import { Component } from "@angular/core";
import { RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from "@angular/router-deprecated";

import { MisRecetasComponent } from "./mis-recetas.component";
import { NuevaRecetaComponent } from "./nueva-receta.component";

@Component({
    selector: "app",
    template: `
        <header>
            <h1>Cookbook</h1>
            <h2>Mi libro de recetas</h2>
        </header>
        <nav>
            <ul>
                <li>
                    <a [routerLink]="['MisRecetas']">{{ enlaceMisRecetas }}</a>
                </li>
                <li>
                    <a [routerLink]="['NuevaReceta']">{{ enlaceNuevaReceta }}</a>
                </li>
            </ul>
        </nav>      
        <router-outlet></router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES],
    providers: [ROUTER_PROVIDERS]
})
@RouteConfig([{
    name: "MisRecetas",
    path: "/mis-recetas",
    component: MisRecetasComponent
}, {
    name: "NuevaReceta",
    path: "/nueva-receta",
    component: NuevaRecetaComponent
}])
export class AppComponent {

    enlaceMisRecetas: string = "Mis Recetas";
    enlaceNuevaReceta: string = "Nueva Receta";
}
