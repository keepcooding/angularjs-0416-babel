
import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import "rxjs/add/operator/map";

@Injectable()
export class ServicioRecetas {
    
    constructor(private _http: Http) { }
    
    obtenerRecetas() {
        return this._http
            .get("http://localhost:9000/api/recetas")
            .map(respuesta => respuesta.json());
    }
}